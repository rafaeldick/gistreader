//
//  File.swift
//  GistReader
//
//  Created by Rafael da Silva Dick on 15/05/17.
//  Copyright © 2017 Rafael da Silva Dick. All rights reserved.
//

import UIKit

class File: NSObject {
    
    var name : String!
    var content : String!
    var rawUrl : String!
    
    override init() {
        
    }

}
