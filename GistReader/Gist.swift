//
//  Gist.swift
//  GistReader
//
//  Created by Rafael da Silva Dick on 10/05/17.
//  Copyright © 2017 Rafael da Silva Dick. All rights reserved.
//

import UIKit

class Gist: NSObject {
    
    var ident : String!
    var htmlUrl : String!
    var desc : String!
    var username : String!
    var gistName : String!
    var arrayFiles : NSMutableArray!
    
    override init() {
        
        self.arrayFiles = NSMutableArray()
    }
    
    func addFile(file : File) {
        self.arrayFiles.add(file)
    }
    
    func getFiles() -> NSArray {
        return self.arrayFiles
    }
}
