//
//  GistFacade.swift
//  GistReader
//
//  Created by Rafael da Silva Dick on 10/05/17.
//  Copyright © 2017 Rafael da Silva Dick. All rights reserved.
//

import UIKit
import Alamofire

class GistFacade: NSObject {
    
    var gist : Gist!
    var networkManager : NetworkManager!
    var idGist: String!
    
    func getGistWithId(idGIst:String, completion:@escaping (Gist) -> ()){
        
        networkManager = NetworkManager()
        
        
        networkManager.getGistWithId(idGist: idGist) { gist in
            
            completion(gist)
            
        }
        
       
        
    }

}
