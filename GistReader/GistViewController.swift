//
//  GistViewController.swift
//  GistReader
//
//  Created by Rafael da Silva Dick on 10/05/17.
//  Copyright © 2017 Rafael da Silva Dick. All rights reserved.
//

import UIKit
import BXProgressHUD

class GistViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate {

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblGistName: UILabel!
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var myTextView: UITextView!
    
    @IBOutlet weak var myScrollView: UIScrollView!
    
    var idGist : String!
    var gistFacade : GistFacade!
    var networkManager : NetworkManager!
    var gist = Gist()
    var fileSelected = File()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let hud = BXHUD.showProgress("Carregando")
        
        self.myTableView.dataSource = self
        self.myTableView.delegate = self
        self.myTextView.delegate = self

        self.myScrollView.contentOffset = CGPoint.init(x: 0, y: 0);
        self.myScrollView.contentSize = CGSize(width: self.myScrollView.frame.size.width, height: self.myScrollView.frame.size.height + 400)
        
        networkManager = NetworkManager()
        
        networkManager.getGistWithId(idGist: idGist) { gist in
            DispatchQueue.main.async {
                
                self.gist = gist
                self.lblUserName.text = gist.username
                self.lblGistName.text = "/ " + gist.gistName
                self.myTableView.reloadData()
            
                BXHUD.hideProgress()
                
            }
        }
        
    }

    // MARK: - Button Action
    @IBAction func btnSendComment(_ sender: Any) {
        
        self.networkManager.createComment(idGist: idGist, comment: self.myTextView.text)
        
    }
    @IBAction func btnCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - TextFieldDelegate
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        let cg = textView.frame
        
        self.myScrollView.setContentOffset(CGPoint(x:cg.origin.x, y: cg.origin.y), animated: true)
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.myScrollView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: true)
    }
    
    // MARK: - TableView Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gist.arrayFiles.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Files"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        let file = self.gist.arrayFiles.object(at: indexPath.row) as! File
        cell.textLabel?.text = file.name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let file = self.gist.arrayFiles.object(at: indexPath.row) as! File
        self.fileSelected = file
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destVC = segue.destination as! WebViewController
        destVC.strUrl = self.fileSelected.rawUrl
    }
    

}
