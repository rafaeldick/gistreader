//
//  NetworkManager.swift
//  GistReader
//
//  Created by Rafael da Silva Dick on 10/05/17.
//  Copyright © 2017 Rafael da Silva Dick. All rights reserved.
//

import UIKit
import Alamofire


class NetworkManager: NSObject {
    
    override init() {
        
    }
    
    
    func getGistWithId(idGist : String, completion: @escaping (Gist) -> Void){
        
        let gist = Gist.init()
        
        Alamofire.request(URL_AUTH + GET_GIST_ID + idGist, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ["access_token":TOKEN]).responseJSON { response in
            
            let result = response.result
            if let dict = result.value as? NSDictionary{
                
                gist.gistName = dict.object(forKey: "description") as! String
                
                if let dictOwner = dict.object(forKey: "owner") as? NSDictionary{
                    gist.username = dictOwner.object(forKey: "login") as! String
                }
                
                if let dictFiles = dict.object(forKey: "files") as? NSDictionary{
                    for var i in 0..<dictFiles.count {
                        let arrayKeys = dictFiles.allKeys as NSArray
                        if let dictFile = dictFiles.object(forKey: arrayKeys.object(at: i)) as? NSDictionary{
                            let file = File()
                            file.name = dictFile.object(forKey: "filename") as! String
                            file.content = dictFile.object(forKey: "content") as! String
                            file.rawUrl = dictFile.object(forKey: "raw_url") as! String
                            gist.addFile(file: file)
                            
                        }
                        i = i + 1
                }
            }
            
        }
            completion(gist)

    }
    }
    
    
    func createComment(idGist : String, comment : String) {
        
        let parameters: [String: String] = [
            "body" : comment as String,
           
        ]
        
        Alamofire.request(URL_AUTH + String(format: CREATE_COMMENT, idGist), method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ["access_token":TOKEN]).responseJSON { response in
            print(response.result)
        }
        
    
    }
    
    
    
    
}
