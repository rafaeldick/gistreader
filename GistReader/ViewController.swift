//
//  ViewController.swift
//  GistReader
//
//  Created by Rafael da Silva Dick on 09/05/17.
//  Copyright © 2017 Rafael da Silva Dick. All rights reserved.
//

import UIKit
import QRCodeController

class ViewController: UIViewController {
    
    let qrCodeController = QRCodeController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib. 72aa4aed0efe2bcf14a02a711462587d
        
        
        qrCodeController.callback = { result in
            print(result)
            self.qrCodeController.dismiss(animated: false, completion: nil)
            
            let navVC = self.storyboard?.instantiateViewController(withIdentifier: "gistVC") as! UINavigationController
            let gistVC = navVC.viewControllers.first as! GistViewController
            gistVC.idGist = result
            
            self.present(navVC, animated: true, completion: nil)
        }   
 
    }

    // MARK: Buttons Actions
    @IBAction func actionBtnRead(_ sender: Any) {
        
        
        self.present(qrCodeController, animated: true, completion: nil)
        

       // present(qrCodeController, animated: true, completion: nil)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

