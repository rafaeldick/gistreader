//
//  WebViewController.swift
//  GistReader
//
//  Created by Rafael da Silva Dick on 15/05/17.
//  Copyright © 2017 Rafael da Silva Dick. All rights reserved.
//

import UIKit

class WebViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var myWebView: UIWebView!
    @IBOutlet weak var myProgressView: UIProgressView!
    var strUrl : String!
    var theBool: Bool!
    var myTimer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.myWebView.delegate = self
        self.myWebView.loadRequest(URLRequest(url: URL(string: self.strUrl)!))

    }
    
    // MARK: - WebView Delegate
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.startWebViewProgress()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.finishWebViewPRogress()
    }

    // mark: - WebView Functions 
    func startWebViewProgress() {
        self.myProgressView.progress = 0.0
        self.theBool = false
        self.myTimer = Timer.scheduledTimer(timeInterval: 0.01667, target: self, selector: #selector(WebViewController.timerCallback), userInfo: nil, repeats: true)
    }
    
    func finishWebViewPRogress() {
        self.theBool = true
    }
    
    func timerCallback() {
        if self.theBool {
            if self.myProgressView.progress >= 1 {
                self.myProgressView.isHidden = true
                self.myTimer.invalidate()
            } else {
                self.myProgressView.progress += 0.1
            }
        } else {
            self.myProgressView.progress += 0.05
            if self.myProgressView.progress >= 0.95 {
                self.myProgressView.progress = 0.95
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
