import XCTest
@testable import GistReaderTests

XCTMain([
    testCase(GistReaderTests.allTests),
])
